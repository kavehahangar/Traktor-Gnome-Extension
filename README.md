# Traktor Gnome Extension

A Gnome Shell Extension to switch the proxy mode between the pre-defined modes "Disable" and "Enable". you can restart tor service via this Extension.

![Screenshot](screenshot.png)

## Installation
```bash
$ git clone https://gitlab.com/GNULand/TraktorPlus/Traktor-Gnome-Extension.git && cd Traktor-Gnome-Extension
$ make build
$ sudo make install
```
## Uninstallation
```bash
$ sudo rm -rf /usr/share/gnome-shell/extensions/Traktor-Extension
$ sudo rm -rf /usr/share/applications/traktor-extension.desktop
```
## Changes
[See Changes](https://gitlab.com/GNULand/TraktorPlus/Traktor-Gnome-Extension/blob/master/CHANGELOG)
